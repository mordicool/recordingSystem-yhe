/**
 * Created by מרדכי on 03 יולי 2016.
 */

$(function () { $('#jstree').jstree({
    "plugins" : [
        "sort",
        "wholerow"
    ],
    'core': {
        'multiple': false,
        'themes': {
            'stripes': true,
            'variant': "large"
        },
		'data': {
			'url': '/jstreedata',
			'dataType': 'json'
		}
    }
})});