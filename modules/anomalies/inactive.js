/**
 * Created by אביה on 28 2019 מרץ.
 */
var fs = require('fs');
var q = require('q');
var config = require('../../config');

var toAlbums = require('../filesToAlbums');

const MS_IN_DAY = 24 * 3600 * 1000

//Defines the types of classes, that's also the timeout
const freqs = config.monitor.inactive.freqs

function freqToString(freq) {
    switch (freq) {
        case freqs.day:
            return "day";
        case freqs.week:
            return "week";
        case freqs.changing:
            return "changing";
        default:
            return "unknown"
    }
}

function filterBatch(times) {
    var result = []

    result[0] = times[0]
    for (var i in times)
        if (Math.abs(times[i] - result[result.length - 1]) > MS_IN_DAY / 2)
            result.push(times[i])

    return result
}

function fixLate(times) {
    if (times[0] > times[1])
        times[0] = times[1]

    var changed
    var counter = 0 //to prevent infinite loop with trash data

    do {
        counter += 1
        changed = false
        for (var i = 1; i < times.length - 1; i++)
            if (times[i + 1] < times[i]) {
                times[i] = (times[i - 1] + times[i + 1]) / 2
                changed = true

            }
    } while (changed && counter < 100)

    return times
}

function chooseFreq(times) {
    const filtered = filterBatch(times)

    const fixedAndSorted = fixLate(filtered)
                            .sort()

    const diff = fixedAndSorted
                    .map((t, idx) => t - fixedAndSorted[idx - 1]).slice(1)
                    .map(i => i / MS_IN_DAY)

    var counts = {}

    counts[freqs.day] = 0
    counts[freqs.week] = 0
    counts[freqs.changing] = 0

    for (var i in diff) {
        if (diff[i] < freqs.day)
            counts[freqs.day]++
        else if (diff[i] < freqs.week)
            counts[freqs.week]++
        else
            counts[freqs.changing]++
    }

    const highest = Number(Object.keys(counts).reduce((a, b) => counts[a] > counts[b] ? a : b))

    //console.log("highest is " + highest)

    return highest
}

function promiseTime(file) {
    return q.nfcall(fs.stat, file)
            .then(s => s.mtimeMs)
    //            .then(t => {
    //                console.log(file,t/MS_IN_DAY)
    //                return t
    //            })
}

function promiseAlbumResult(album) {
    return q.all(album.files.map(f => promiseTime(f)))
            .then(times => {
                return {
                    name: album.name,
                    daysPassed: Math.round((Date.now() - Math.max.apply(null, times)) / MS_IN_DAY),
                    freq: chooseFreq(times)
                }
            });
}

function FindInactive(files) {
    return q.all(
                toAlbums(files)
                    .map(album => promiseAlbumResult(album))
                )
            .then(albumsResult => albumsResult
                .filter(ar => ar.daysPassed > ar.freq)
                .filter(ar => ar.daysPassed < config.monitor.inactive.stopTrackingInactiveAfter)
                .map(ar => {
                    ar.typeOfClass = freqToString(ar.freq)
                    delete ar.freq
                    return ar
                })
                .sort((a, b) => a.daysPassed - b.daysPassed)
            );
}

module.exports = FindInactive;


/*
function minus(arr1, arr2) {
    var arr3 = []

    for (var i in arr1)
        arr3[i] = arr1[i] - arr2[i]

    console.log(arr3)
}

function printNormalize(arr) {
    var arr3 = []

    for (var i in arr)
        arr3[i] = (arr[i] - arr[0]) / MS_IN_DAY

    console.log(arr3)
}
*/