/**
 * Created by אביה on 23 אוגוסט 2018.
 */

function FindNonMp3(files) {
    return files.filter(f => !f.endsWith('.mp3'));
}

module.exports = FindNonMp3;