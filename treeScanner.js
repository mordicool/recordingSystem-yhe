path = require("path")
fs = require("fs")
config = require("./config")

function populateTree(tree, p)
{
    files = fs.readdirSync(p)
    
    var children = []
    
    files.map(function (file) {
        return path.join(p, file) // so I get whole file names
    }).filter(function (file) {
        return fs.statSync(file).isDirectory()
    }).forEach(function (dir) {
        populateTree(children, dir)
    })
    
    if (children.length < 1) {
        tree.push(path.basename(p))
    }
    else {
        var me = {}
        me.text = path.basename(p)
        me.state = {
            'disabled': true
        }
        me.children = children
        tree.push(me)
    }
}

exports.getTree = function() {
	scan_path = config.uploadsFolder
	body = []
	populateTree(body, scan_path)
	if (!config.dirScanner.showTopLevel)
		body = body[0].children
	return body
}
