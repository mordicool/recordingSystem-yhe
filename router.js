/**
 * Created by מרדכי on 09 אוגוסט 2016.
 */

var config = require('./config');
var logger = require('./modules/logger');
var fileHandler = require('./modules/fileHandler');
var tracker = require('./modules/trackDirChanges');
var express = require('express');
var router = express.Router();
var q = require('q');
var fs = require('fs');
var path = require('path');

var routeMonitor = require('./routeMonitor');

router.get('/', function (req, res) {
    res.redirect('/index.html');
});

router.post('/api/uploadFile', function (req, res) {
    q.fcall(fileHandler.handleFile, req)
    .then(function (filePath) {
        logger.info('New record arrived. File name: ' + filePath);
        res.sendStatus(200);
    })
    .fail(function (error) {
        logger.error('Error while receiving a record. Error message: ' + error.message);
        res.sendStatus(500);
    })
    .done();
});

router.get('/jstreedata', function (req, res) {
    tracker.getJstreeData()
        .then(result => res.json(result))
        .fail(err => res.sendStatus(500))
})

router.get('/api/refreshFolderList', function (req, res) {
    tracker.refreshFolderList()
    res.sendStatus(200)
})

router.get('/info', function (req, res) {
    res.sendFile(path.resolve(__dirname, config.logger.infoLogFileName));
});

router.get('/debug', function (req, res) {
    res.sendFile(path.resolve(__dirname, config.logger.debugLogFileName));
});

routeMonitor(router);

module.exports = router;